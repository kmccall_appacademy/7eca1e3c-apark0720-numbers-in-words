class Integer
  def in_words
    scales = {
      1000000000000 => "trillion",
      1000000000 => "billion",
      1000000 => "million",
      1000 => "thousand",
      100 => "hundred"
    }
    tens = {
      9 => "ninety",
      8 => "eighty",
      7 => "seventy",
      6 => "sixty",
      5 => "fifty",
      4 => "forty",
      3 => "thirty",
      2 => "twenty"
    }
    teens = {
      9 => "nineteen",
      8 => "eighteen",
      7 => "seventeen",
      6 => "sixteen",
      5 => "fifteen",
      4 => "fourteen",
      3 => "thirteen",
      2 => "twelve",
      1 => "eleven",
      0 => "ten"
    }
    singles = {
      9 => "nine",
      8 => "eight",
      7 => "seven",
      6 => "six",
      5 => "five",
      4 => "four",
      3 => "three",
      2 => "two",
      1 => "one",
    }

    output, int = [], self

    scales.each do |scale, name|
      next if int < scale
      divided = int / scale
      output << "#{divided.in_words} #{name}"
      int = int % scale
    end

    int = int.to_s.chars.map(&:to_i) #[1,3,5]

    if int.length == 2
      if int[0] == 1
        output << teens[int[1]]
      elsif int[1] == 0
        output << tens[int[0]]
      else
        output << "#{tens[int[0]]} #{singles[int[1]]}"
      end
    else
      if int[0] == 0
        return "zero" if output.empty?
      else
        output << singles[int[0]]
      end
    end

    output.join(" ")
  end
end
